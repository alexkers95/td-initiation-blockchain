import hashlib
import getpass
from datetime import datetime

##fonction de récupération du wallet par email
def getWallet(email):
    with open("transactions.csv", 'r') as read_obj:
        lines = read_obj.readlines()
        lines = [line.strip() for line in lines]
        lines = reversed(lines)
        isOk = 0
        for line in lines:
            ownerEmail, wallet, date = line.split(",")
            if email == ownerEmail:
                isOk = 1
                return int(wallet)
        if isOk == 0:
            return 0
        

##fonction d'inscription
def signup():
    email = input("Enter email address: ")
    pwd = getpass.getpass("Enter password: ")
    conf_pwd = getpass.getpass("Confirm password: ")     

    if conf_pwd == pwd:
        enc = conf_pwd.encode()
        hash1 = hashlib.md5(enc).hexdigest()     

        with open("credentials.csv", "a") as f:
            f.write(email + ",")
            f.write(hash1 + "\n")
        f.close()
        with open("transactions.csv", "a") as f:
            f.write(email + ",")
            f.write("10" + ",")
            f.write(str(datetime.now()) +"\n")
        f.close()
        print("connected !")     
    
    else :
        print("Password did not match \n")

#fonction de connexion
def login():
     email = input("Enter email: ")
     pwd = getpass.getpass("Enter password: ")     
     auth = pwd.encode()
     wallet = 0
     auth_hash = hashlib.md5(auth).hexdigest()
     with open("credentials.csv", "r") as n:
        lines = n.readlines()
        logged = 0
        for line in lines:
            stored_email, stored_pwd = line.split(",")
            if email == stored_email and auth_hash == stored_pwd.strip():
                logged = 1
                wallet = getWallet(str(email))
                print("connected acount = ", str(wallet), " bitcoin \n")

        n.close() 

        if logged == 0:
            print("Login failed! \n")
            return 0
     return email, wallet

#fonction d'envoi d'argent
def sendMoney():
    log = login()
    if log==0:
        sendMoney()
    else:
        sender, accountAmount = log

        amount = int(input("enter amount to send: "))
        if amount<0:
            print("vous ne pouvez pas envoyer un montant negatif ")
        elif accountAmount < amount:
            print("vous n'avez pas assez sur votre compte ")
        else:
            receiver = input("enter receiver: ")
            with open("credentials.csv", "r") as f:
                lignes = f.readlines()
                for line in lignes:
                    stored_email, stored_pwd = line.split(",")
                    if receiver == stored_email:
                        receiverWallet= getWallet(receiver)
                        receiverWallet += amount
                        ownerWallet = accountAmount - amount
                        with open("transactions.csv", "a") as p:
                            p.write(sender + ",")
                            p.write(str(ownerWallet) + ",")
                            p.write(str(datetime.now()) +"\n")
                            p.write(receiver + ",")
                            p.write(str(receiverWallet) + ",")
                            p.write(str(datetime.now()) +"\n")
                        print("money sent ! \n")
                f.close()   

#fonction qui affiche l'etat actuel du wallet d'un utilisateur en fonction de son adresse mail
def checkWallet():
    email = input("Enter wallet owner email: ")
    currentWallet = getWallet(email)
    print(email,"\'s wallet contains ", currentWallet, " bitcoin \n")



#Interface utilisateur
while 1:
    print("********** App Menu **********")
    print("1.Signup")
    print("2.send money")
    print("3.check wallet status by email")
    print("4.Exit")
    ch = int(input("Enter your choice: "))
    if ch == 1:
        signup()
    elif ch == 2:
        sendMoney()
    elif ch == 3:
        checkWallet()
    elif ch == 4:
        break
    else:
        print("Wrong Choice!")